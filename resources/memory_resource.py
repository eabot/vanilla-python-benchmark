import json, re
import falcon


class MemoryResource:
    def on_get(self, req, resp):
        """Handles GET requests"""

        with open('ram_test.txt', 'r') as myfile:
            test_str = myfile.read()

        regex = r"[aeiouAEIOU]"

        matches = re.findall(regex, test_str)

        resp.status = falcon.HTTP_OK
        resp.media = json.dumps({'n_vowels': len(matches)})
