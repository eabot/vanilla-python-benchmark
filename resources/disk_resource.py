import json, re
from tempfile import NamedTemporaryFile

import falcon


class DiskResource:
    def on_get(self, req, resp):
        """Handles GET requests"""

        with open('disk_test.csv', 'r') as the_origin:
            test_str = the_origin.read()
            the_origin.close()

        with NamedTemporaryFile(delete=True) as the_destination:
            bytes_written = the_destination.write(test_str.encode())
            the_destination.close()

        resp.status = falcon.HTTP_OK
        resp.media = json.dumps({'bytes': bytes_written})
