import falcon

from resources.cpu_resource import CpuResource
from resources.disk_resource import DiskResource
from resources.memory_resource import MemoryResource
from resources.throughput_resource import ThroughputResource


class BenchmarkServiceBuilder:
    @staticmethod
    def build():
        api = falcon.API()
        api.add_route('/ram', MemoryResource())
        api.add_route('/throughput', ThroughputResource())
        api.add_route('/cpu', CpuResource())
        api.add_route('/disk', DiskResource())

        return api
