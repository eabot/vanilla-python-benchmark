from wsgiref.simple_server import make_server
from benchmark_service import BenchmarkServiceBuilder

APP = BenchmarkServiceBuilder.build()

if __name__ == '__main__':
    httpd = make_server('', 8000, APP)
    print("Serving on port 8000...")
    httpd.serve_forever()